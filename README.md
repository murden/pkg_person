# Person Package __ 33Robotics 


## Instalação

```bash
npm install 33robotics_person@latest
```

## Importar as rotas no Vue Router (@/router/index.js)
```javascript
import personRouterMap from '33robotics_person/src/router'

export const constantRouterMap = [{
    path: '/login',
    name: 'Login 33Robotics',
    component: () =>
      import ('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    name: '404',
    component: () =>
      import ('@/views/404'),
    hidden: true
  },
  { path: '*', redirect: '/404', hidden: true },
  ...personRouterMap
]

export default new Router({
  // mode: 'history', //Backend support can be opened
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
```

## Importar a store no Vuex (@/store/index.js)
```javascript
import personStore from '33robotics_person/src/store'

const store = new Vuex.Store({
  modules: {
    ...personStore
  },
  getters
  // plugins: [vuexLocal.plugin]
})
```

## Teste local
```bash
npm run dev
```