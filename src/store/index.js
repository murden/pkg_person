import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import usuario from '../modules/usuario/store'
import contact from '../modules/contact/store'
import companyGroup from '../modules/companyGroup/store'
import company from '../modules/company/store'
import department from '../modules/department/store'
import costCenter from '../modules/costCenter/store'
import type from '../modules/type/store'
import classes from '../modules/classes/store'
import right from '../modules/right/store'
import rightGroup from '../modules/rightGroup/store'
import rightControl from '../modules/rightControl/store'
import country from '../modules/country/store'
import uf from '../modules/uf/store'
import city from '../modules/city/store'

// import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

// const vuexLocal = new VuexPersistence({
//   storage: window.localStorage
// })

export const personStore = {
  app,
  user,
  usuario,
  contact,
  companyGroup,
  company,
  department,
  costCenter,
  type,
  classes,
  right,
  rightGroup,
  rightControl,
  country,
  uf,
  city
}

export default personStore
