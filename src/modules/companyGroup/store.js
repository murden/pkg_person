import { CRUD } from '../_common/store/crud'
import Service from '../_common/api/BaseService'

const crud = CRUD(new Service('/default/CompanyGroup'))

const companyGroup = {
  namespaced: true,
  modules: {
    crud
  },
  state: {},
  getters: {},
  mutations: {},
  actions: {}
}

export default companyGroup
