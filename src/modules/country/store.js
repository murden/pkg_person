import { CRUD } from '../_common/store/crud'
import Service from '../_common/api/BaseService'

const crud = CRUD(new Service('/default/Country'))

const country = {
  namespaced: true,
  modules: {
    crud
  },
  state: {},
  getters: {},
  mutations: {},
  actions: {}
}

export default country
