import { CRUD } from '../_common/store/crud'
import Service from '../_common/api/BaseService'

const crud = CRUD(new Service('/default/Person'))

const usuario = {
  namespaced: true,
  modules: {
    crud
  },
  state: {},
  getters: {},
  mutations: {},
  actions: {}
}

export default usuario
