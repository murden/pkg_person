/** @param {BaseService} Service */
const CRUD = (Service) => {
  /** @template T */
  const state = {
    /**
     * @type {T[]}
     * Lista de todos os {T}
     */
    items: [],
    /**
     * @type {T}
     * Item que se está trabalhando
     */
    selectedItem: undefined,
    page: 1,
    total: 0,
    from: 0,
    to: 0,
    lastPage: 0,
    loading: false
  }

  const getters = {
    items: (state) => state.items,
    selectedItem: (state) => state.selectedItem,
    loading: (state) => state.loading,
    page: (state) => state.page,
    from: (state) => state.from,
    to: (state) => state.to,
    lastPage: (state) => state.lastPage,
    total: (state) => state.total
  }

  /** @type {import("vuex").MutationTree<typeof state>} */
  const mutations = {
    SET_ITEMS(state, payload) {
      state.items = payload
    },
    SET_SELECTED_ITEM(state, payload) {
      state.selectedItem = payload
    },
    SET_LOADING(state, payload) {
      state.loading = payload
    },
    SET_PAGE(state, payload) {
      state.page = payload
    },
    SET_FROM(state, payload) {
      state.from = payload
    },
    SET_TO(state, payload) {
      state.to = payload
    },
    SET_TOTAL(state, payload) {
      state.total = payload
    }
  }

  /** @type {import("vuex").ActionTree<typeof state>} */
  const actions = {
    /**
     * Buscar lista de itens e adicionar o resultado na state list
     * @param {*} context
     */

    setLoading(context) {
      context.commit('SET_LOADING', true)
    },
    find(context) {
      context.commit('SET_LOADING', true)
      return Service.find()
        .then((response) => context.commit('SET_ITEMS', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     * Buscar lista de itens e adicionar o resultado na state list
     * @param {*} context
     */

    findPaginated(context, { page, created_at_ini, created_at_fim, datanascimento_ini, datanascimento_fim, nome, name, descricao, genero, peso_min, peso_max, altura_min, altura_max, cintura_min, cintura_max, gordura_min, gordura_max, musculacao, tipodieta }) {
      page = page || 1

      const query = {
        page
      }
      if (created_at_ini) {
        query.created_at_ini = created_at_ini
      }
      if (created_at_fim) {
        query.created_at_fim = created_at_fim
      }
      if (datanascimento_ini) {
        query.datanascimento_ini = datanascimento_ini
      }
      if (datanascimento_fim) {
        query.datanascimento_fim = datanascimento_fim
      }
      if (name) {
        query.name = name
      }
      if (nome) {
        query.nome = nome
      }
      if (descricao) {
        query.descricao = descricao
      }
      if (genero) {
        query.genero = genero
      }
      if (peso_min) {
        query.peso_min = peso_min
      }
      if (peso_max) {
        query.peso_max = peso_max
      }
      if (altura_min) {
        query.altura_min = altura_min
      }
      if (altura_max) {
        query.altura_max = altura_max
      }
      if (cintura_min) {
        query.cintura_min = cintura_min
      }
      if (cintura_max) {
        query.cintura_max = cintura_max
      }
      if (gordura_min) {
        query.gordura_min = gordura_min
      }
      if (gordura_max) {
        query.gordura_max = gordura_max
      }
      if (musculacao) {
        query.musculacao = musculacao
      }
      if (tipodieta) {
        query.tipodieta = tipodieta
      }
      context.commit('SET_LOADING', true)
      context.commit('SET_PAGE', page)
      return Service.find(query)
        .then((response) => {
          context.commit('SET_TOTAL', response.data.total)
          context.commit('SET_FROM', response.data.from)
          context.commit('SET_TO', response.data.to)
          context.commit('SET_ITEMS', response.data.data)
          console.log(response.data)
        })
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     * Executa o find passando parametros de query na rota T/params
     * @param {*} context
     */
    params(context, payload) {
      context.commit('SET_LOADING', true)
      return Service.params(payload)
        .then((response) => context.commit('SET_ITEMS', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    search(context, payload) {
      context.commit('SET_LOADING', true)
      return Service.search(payload)
        .then((response) => response.data)
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     *  Enviar requisição para adicionar um novo item na base
     * @param {*} context
     * @param {T} payload
     */
    create(context, payload) {
      context.commit('SET_LOADING', true)
      return Service.create(payload)
        .then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     *  Enviar requisição para atualizar ou inserir item na base
     * @param {*} context
     * @param {T} payload
     */
    upsert(context, payload) {
      context.commit('SET_LOADING', true)
      let promise = null
      if (payload.id) {
        const { id } = payload
        delete payload.id
        promise = Service.update(id, payload)
      }
      else {
        promise = Service.create(payload)
      }
      return promise
        .then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     * Enviar requisição para atualizar ou inserir item na base, onde payload contem informações
     * de repaginação.
     * @param {*} context
     * @param {T} payload Payload contendo os campos { item, query }
     */
    upsertPaginated(context, { item, query }) {
      context.commit('SET_LOADING', true)
      let promise = null
      if (item.id) {
        const { id } = item
        delete item.id
        promise = Service.update(id, item)
      }
      else {
        promise = Service.create(item)
      }
      return promise
        .then(() => context.dispatch('findPaginated', query))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     * Enviar requisição para atualizar um item na base
     * O payload precisa conter 2 objetos, um com o id do item que está sendo editado, outro com os dados
     * @param {*} context
     * @param {*} payload
     */
    update(context, { id, data }) {
      context.commit('SET_LOADING', true)
      return Service.update(id, data)
        .then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     * Remover o item {T}
     * @param {*} context
     * @param {T} payload
     */
    delete(context, { id }) {
      context.commit('SET_LOADING', true)
      return Service.delete(id)
        .then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     *  Seleciona um item {T}
     * @param {*} context
     * @param {T} payload
     */
    select(context, { id }) {
      context.commit('SET_LOADING', true)
      return Service.findOne(id)
        .then((response) => response.data)
        .then((data) => {
          context.commit('SET_SELECTED_ITEM', data)
          return data
        })
        .finally(() => context.commit('SET_LOADING', false))
    }
  }

  return {
    state,
    getters,
    mutations,
    actions
  }
}

export { CRUD }
