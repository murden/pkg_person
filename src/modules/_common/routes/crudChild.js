const crudRoutes = (title, name, icon) => {
  return [{
      path: `/${name}/list`,
      name: `${name}_list`,
      component: () =>
        import (`33robotics_person/src/modules/${name}/views/index`),
      meta: { title: title, noCache: true, icon: icon }
    },
    {
      path: `/${name}/new`,
      name: `${name}_new`,
      hidden: true,
      component: () =>
        import (`33robotics_person/src/modules/${name}/views/upsert`),
      meta: { title: `Cadastro de ${title}`, noCache: true }
    },
    {
      path: `/${name}/:id`,
      name: `${name}_edit`,
      hidden: true,
      component: () =>
        import (`33robotics_person/src/modules/${name}/views/upsert`),
      meta: { title: `Editar ${title}`, noCache: true }
    }
  ]
}

export { crudRoutes }
