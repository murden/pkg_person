export default {
  methods: {
    /**
     * Redireciona para a rota de edicao do item
     * @param {object} item item a ser editado
     */
    handleUpdate(item) {
      this.$router.push(`${item.id}`)
    },
    /**
     * Remove o item, necessita da confirmacao do usuario
     * @param {object} item item a ser removido
     */
    handleDelete(item) {
      this.$confirm(
          'Esta ação é permanente. Continuar mesmo assim?',
          'Remover cadastro?', {
            confirmButtonText: 'Remover',
            cancelButtonText: 'Cancelar',
            // type: 'error',
            confirmButtonClass: 'el-button--primary',
            iconClass: 'el-icon-delete el-button--mini el-button--primary is-circle iconMessageBox',
            center: true
          })
        .then((ok) => ok ? this.delete(item) : false)
        .then(() => this.$notify({
          title: 'Item excluido',
          message: 'OK',
          type: 'success'
        }))
        .catch(console.log)
    },
    /**
     * Valida e submete o formulario $refs['dataForm'], utiliza o this.item para enviar para upsert
     */
    submit() {
      this.$refs['dataForm'].clearValidate()
      this.$refs['dataForm'].validate(valid => {
        if (valid) {
          this.upsert(this.item)
            .then(() => this.$notify({ title: 'Item atualizado', message: 'OK', type: 'success', duration: 2000 }))
            .then(() => this.$router.go(-1))
            .catch((err) => this.$notify({ title: 'Ocorreu um erro', message: err, type: 'error', duration: 2000 }))
        }
        else {
          this.$notify({ title: 'Revise os campos', message: 'Erro na validacao', type: 'error', duration: 2000 })
        }
      })
    },
    /**
     * Valida e submete o formulario $refs['dataForm'], utiliza o this.item para enviar o request.
     * Assume que a ação de sincronização deve utilizar busca com paginação.
     */
    submitPaginated() {
      this.$refs['dataForm'].clearValidate()
      this.$refs['dataForm'].validate(valid => {
        if (valid) {
          this.upsertPaginated({ item: this.item, query: { page: this.page, name: this.name, descricao: this.descricao, nome: this.nome } })
            .then(() => this.$notify({ title: 'Item atualizado', message: 'OK', type: 'success', duration: 2000 }))
            .then(() => this.$router.go(-1))
            .catch((err) => this.$notify({ title: 'Ocorreu um erro', message: err, type: 'error', duration: 2000 }))
        }
        else {
          this.$notify({ title: 'Revise os campos', message: 'Erro na validação', type: 'error', duration: 2000 })
        }
      })
    }
  }
}
