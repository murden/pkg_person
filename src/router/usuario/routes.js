import Layout from '@/views/layout/Layout'
import usuario from '../../modules/usuario/routes'
import contact from '../../modules/contact/routes'
import classes from '../../modules/classes/routes'
import type from '../../modules/type/routes'

export const personRoutes = {
  path: '/usuarios',
  component: Layout,
  redirect: '/usuario/list',
  name: 'Gestão de Pessoas',
  meta: { title: 'Gestão de Pessoas', icon: 'face' },
  children: [
    ...usuario,
    ...contact,
    ...type,
    ...classes
  ]
}

export default personRoutes
