import Layout from '@/views/layout/Layout'
import department from '../../modules/department/routes'
import company from '../../modules/company/routes'
import companyGroup from '../../modules/companyGroup/routes'
import costCenter from '../../modules/costCenter/routes'

export const companyRoutes = {
  path: '/companies',
  component: Layout,
  redirect: '/company/list',
  name: 'Gestão de Empresas',
  meta: { title: 'Gestão de Empresas', icon: 'company' },
  children: [
    ...department,
    ...company,
    ...companyGroup,
    ...costCenter,
  ]
}

export default companyRoutes
