import Layout from '@/views/layout/Layout'
import right from '../../modules/right/routes'
import rightGroup from '../../modules/rightGroup/routes'
import rightControl from '../../modules/rightControl/routes'

export const rightRoutes = {
  path: '/rights',
  component: Layout,
  redirect: '/rightGroup/list',
  name: 'Cadastros de Direitos',
  meta: { title: 'Gestão de Direitos', icon: 'security' },
  children: [
    ...rightControl,
    ...right,
    ...rightGroup
  ]
}

export default rightRoutes
