import Layout from '@/views/layout/Layout'
import country from '../../modules/country/routes'
import uf from '../../modules/uf/routes'
import city from '../../modules/city/routes'

export const locationRoutes = {
  path: '/location',
  component: Layout,
  redirect: '/city/list',
  name: 'Cadastros de Local',
  meta: { title: 'Cadastro de Local', icon: 'locale' },
  children: [
    ...city,
    ...uf,
    ...country
  ]
}

export default locationRoutes
